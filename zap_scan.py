import time
import json
from zapv2 import ZAPv2

# Настройка прокси-сервера для подключения к ZAP
zap = ZAPv2(apikey='', proxies={'http': 'http://zap:8080', 'https': 'http://zap:8080'})

# Целевая веб-приложение
target = 'http://juice_shop:3000'

# Сообщение о начале доступа к целевому веб-приложению
print(f'Accessing target {target}')
zap.urlopen(target)
time.sleep(2)  # Даем время для загрузки страницы

# Сообщение о начале пассивного сканирования
print('Starting passive scan')
zap.pscan.enable_all_scanners()

# Сообщение о начале активного сканирования
print(f'Starting active scan on target {target}')
scan_id = zap.ascan.scan(target)

# Проверка прогресса активного сканирования
while int(zap.ascan.status(scan_id)) < 100:
    print(f'Active Scan progress: {zap.ascan.status(scan_id)}%')
    time.sleep(5)

# Сообщение о завершении активного сканирования
print('Active Scan completed')

# Получение отчетов о найденных предупреждениях
alerts = zap.core.alerts()

# Сохранение отчета в текущую рабочую директорию
report_path = 'zap_report.json'
with open(report_path, 'w') as f:
    json.dump(alerts, f, indent=4)

# Сообщение о сохранении отчета
print(f'Report saved to {report_path}')
