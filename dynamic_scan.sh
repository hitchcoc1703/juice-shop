#!/bin/bash
# dynamic_scan.sh
# Скрипт для динамического сканирования веб-приложения

echo "Начало динамического сканирования..."
# Запуск сканера. Предположим, что он сохраняет результаты в dynamic_scan_report.json
dynamic-scanner --url http://juice_shop:3000 --output dynamic_scan_report.json
if [ $? -ne 0 ]; then
    echo "Ошибка при выполнении динамического сканирования"
    exit 1
fi
echo "Динамическое сканирование завершено"