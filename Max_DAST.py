import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin
import re

# Глобальные переменные
visited_urls = set()
found_vulnerabilities = []


# Вспомогательная функция для вывода отчетов в файл
def print_report(file_path="vulnerability_report.txt"):
    with open(file_path, "w", encoding="utf-8") as f:
        f.write("[ОТЧЕТ О УЯЗВИМОСТЯХ]\n")
        if not found_vulnerabilities:
            f.write("Уязвимости не найдены.\n")
        else:
            for i, vulnerability in enumerate(found_vulnerabilities, 1):
                f.write(f"\n\nНомер уязвимости: {i}\n")
                f.write(f"Уязвимость: {vulnerability['type']}\n")
                f.write(f"URL: {vulnerability['url']}\n")
                f.write(f"Описание: {vulnerability['description']}\n")
                f.write(f"Чем опасна: {vulnerability['danger']}\n")
                f.write(f"Рекомендации: {vulnerability['remedy']}\n")


# Функция для проверки уязвимости №1: Broken Access Control (Нарушение контроля доступа)
def check_broken_access_control(url):
    """Проверяет уязвимость Broken Access Control (Нарушение контроля доступа), посылая запросы к защищенным страницам."""

    protected_urls = ['/admin', '/config', '/settings']
    for protected_url in protected_urls:
        full_url = urljoin(url, protected_url)
        response = requests.get(full_url)
        if response.status_code == 200:
            found_vulnerabilities.append({
                'type': 'Broken Access Control',
                'url': full_url,
                'description': 'Нарушение контроля доступа, возможность получить доступ к защищенным страницам без аутентификации.',
                'danger': 'Злоумышленник может получить доступ к конфиденциальным данным или сделать изменения на сайте',
                'remedy': 'Используйте методы контроля доступа и убедитесь, что защищенные страницы требуют аутентификации'
            })


# Функция для проверки уязвимости №2: Cryptographic Failures (Криптографические ошибки)
def check_cryptographic_failures(url):
    """Проверка использования HTTPS."""

    if not url.startswith('https://'):
        found_vulnerabilities.append({
            'type': 'Cryptographic Failures',
            'url': url,
            'description': 'Используется незащищенное соединение HTTP.',
            'danger': 'Данные пользователя могут быть перехвачены злоумышленниками',
            'remedy': 'Используйте HTTPS вместо HTTP для защиты данных пользователя'
        })


# Функция для проверки уязвимости №3: Injection (Инъекции)
def check_injection(url):
    """Проверка уязвимости инъекций на сайте, выполняя простую проверку SQL-инъекций."""

    test_url = urljoin(url, "/product?id=1' OR '1'='1")
    response = requests.get(test_url)
    if "syntax error" in response.text.lower() or "mysql" in response.text.lower():
        found_vulnerabilities.append({
            'type': 'Injection',
            'url': test_url,
            'description': 'Уязвимость SQL-инъекции обнаружена.',
            'danger': 'Злоумышленник может выполнить произвольные SQL-запросы к базе данных',
            'remedy': 'Используйте параметризованные запросы и ORM для работы с базой данных'
        })


# Функция для проверки уязвимости №4: Insecure Design (Небезопасный дизайн)
def check_insecure_design(url):
    """Проверка идей и методов дизайна сайта, которые могут быть небезопасными."""

    # Эта уязвимость требует ручного анализа
    found_vulnerabilities.append({
        'type': 'Insecure Design',
        'url': url,
        'description': 'Потенциально небезопасный дизайн сайта.',
        'danger': 'Злоумышленник может воспользоваться уязвимостями в архитектуре сайта',
        'remedy': 'Проведите анализ дизайна и архитектуры сайта для выявления и устранения слабых мест'
    })


# Функция для проверки уязвимости №5: Security Misconfiguration (Неправильная настройка безопасности)
def check_security_misconfiguration(url):
    """Проверка признаков неправильной настройки безопасности, например, доступности административных панелей."""

    admin_urls = ['/admin', '/administrator', '/login']
    for admin_url in admin_urls:
        full_url = urljoin(url, admin_url)
        response = requests.get(full_url)
        if response.status_code == 200:
            found_vulnerabilities.append({
                'type': 'Security Misconfiguration',
                'url': full_url,
                'description': 'Обнаружена доступная административная панель.',
                'danger': 'Злоумышленник может получить несанкционированный доступ к системе управления сайтом',
                'remedy': 'Закройте доступ к административной панели для неавторизованных пользователей'
            })


# Функция для проверки уязвимости №6: Vulnerable and Outdated Components (Уязвимые и устаревшие компоненты)
def check_vulnerable_and_outdated_components(url):
    """Проверка использования известных уязвимых или устаревших компонентов."""

    response = requests.get(url)
    if 'X-Powered-By' in response.headers:
        server_info = response.headers['X-Powered-By']
        # Простая проверка на устаревшие версии
        if 'PHP/5' in server_info or 'ASP.NET/2.0' in server_info:
            found_vulnerabilities.append({
                'type': 'Vulnerable and Outdated Components',
                'url': url,
                'description': 'Используется устаревшая версия сервера.',
                'danger': 'Устаревшие версии могут содержать многочисленные уязвимости',
                'remedy': 'Обновите используемые компоненты до актуальных версий'
            })


# Функция для проверки уязвимости №7: Identification and Authentication Failures (Ошибки идентификации и аутентификации)
def check_identification_and_authentication_failures(url):
    """Проверка правильности реализации механизмов идентификации и аутентификации, например, наличие CAPTCHA."""

    login_urls = ['/login', '/signin', '/auth']
    for login_url in login_urls:
        full_url = urljoin(url, login_url)
        response = requests.get(full_url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            if not soup.find('input', attrs={'type': 'captcha'}):
                found_vulnerabilities.append({
                    'type': 'Identification and Authentication Failures',
                    'url': full_url,
                    'description': 'На странице входа отсутствует CAPTCHA.',
                    'danger': 'Отсутствие CAPTCHA может позволить злоумышленникам проводить атаки методом грубой силы',
                    'remedy': 'Добавьте CAPTCHA на страницу входа для перебора паролей'
                })


# Функция для проверки уязвимости №8: Software and Data Integrity Failures (Сбой целостности ПО и данных)
def check_software_and_data_integrity_failures(url):
    """Проверяется наличие подписи содержимого, проверки целостности и CSRF токена."""

    response = requests.get(url)
    if 'Integrity' not in response.headers:
        found_vulnerabilities.append({
            'type': 'Software and Data Integrity Failures',
            'url': url,
            'description': 'Отсутствуют механизмы проверки целостности.',
            'danger': 'Недостаток проверки целостности может привести к изменению данных',
            'remedy': 'Включите механизмы проверки целостности данных и программного обеспечения'
        })


# Функция для проверки уязвимости №9: Security Logging and Monitoring Failures (Ошибки ведения журналов и мониторинга безопасности)
def check_security_logging_and_monitoring_failures(url):
    """Проверяется наличие сервисов журналирования и мониторинга, например, страницы /log не должна быть доступна."""

    log_urls = ['/log', '/logs', '/logging']
    for log_url in log_urls:
        full_url = urljoin(url, log_url)
        response = requests.get(full_url)
        if response.status_code == 200 and "log" in response.text.lower():
            found_vulnerabilities.append({
                'type': 'Security Logging and Monitoring Failures',
                'url': full_url,
                'description': 'Доступен открытый журнал логов.',
                'danger': 'Злоумышленник может получить доступ к логам и использовать их для дальнейших атак',
                'remedy': 'Закройте доступ к журналам логов или используйте ограничение доступа'
            })


# Функция для проверки уязвимости №10: Server-Side Request Forgery (SSRF) (Подделка запросов от имени сервера)
def check_ssrf(url):
    """Проверяется возможность выполнения SSRF атак."""

    payload_url = urljoin(url, "/?url=http://localhost")
    response = requests.get(payload_url)
    if response.status_code == 200:
        found_vulnerabilities.append({
            'type': 'Server-Side Request Forgery (SSRF)',
            'url': payload_url,
            'description': 'Сервер может выполнять запрос к произвольному URL.',
            'danger': 'Злоумышленник может использовать SSRF для доступа к внутренним системам',
            'remedy': 'Ограничьте возможность выполнения запросов к произвольным URL с сервера'
        })


# Основная функция сканирования сайта
def scan_website(url):
    print(f"Сканирование сайта: {url}")
    visited_urls.add(url)
    response = requests.get(url)
    if response.status_code != 200:
        print(f"Не удалось получить доступ к URL: {url}")
        return

    # Использование BeautifulSoup для парсинга HTML документов
    soup = BeautifulSoup(response.text, 'html.parser')

    # Проверка всех уязвимостей из OWASP TOP 10 2021
    check_broken_access_control(url)
    check_cryptographic_failures(url)
    check_injection(url)
    check_insecure_design(url)
    check_security_misconfiguration(url)
    check_vulnerable_and_outdated_components(url)
    check_identification_and_authentication_failures(url)
    check_software_and_data_integrity_failures(url)
    check_security_logging_and_monitoring_failures(url)
    check_ssrf(url)

    # Рекурсивный обход ссылок на странице
    for link in soup.find_all("a"):
        href = link.get("href")
        if href:
            full_url = urljoin(url, href)
            domain = urlparse(full_url).netloc
            if full_url not in visited_urls and domain == urlparse(url).netloc:
                scan_website(full_url)


# Ввод URL для проверки
website_url = input("Введите URL сайта для сканирования: ")

# Запуск процесса сканирования
scan_website(website_url)
print_report()

print("Сканирование завершено. Отчет сохранен в vulnerability_report.txt")